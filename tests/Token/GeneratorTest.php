<?php

namespace ConferenciaCorp\Autenticacao\Token;

use ConferenciaCorp\Autenticacao\Utils\Common;
use ConferenciaCorp\Autenticacao\Builder\TokenBuilder;
use PHPUnit_Framework_TestCase as PHPUnit;

class GeneratorTest extends PHPUnit
{
    private $secretKey;
    private $alg;

    public function setUp()
    {
        $this->tokenBuilder = new TokenBuilder(Common::SECRET_KEY, Common::ALG);
    }

    public function testShouldGenerateAToken()
    {
        $token = $this->tokenBuilder->get();
        $this->assertInternalType('string', $token);
    }

    public function testShouldGenerateATokenWithExpiryDate()
    {
        $token = $this->tokenBuilder->getWithExpiryDate();
        $this->assertInternalType('string', $token);
    }
}