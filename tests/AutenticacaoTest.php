<?php

namespace ConferenciaCorp\Autenticacao;

use ConferenciaCorp\Autenticacao\Utils\Common;
use ConferenciaCorp\Autenticacao\Builder\TokenBuilder;
use ConferenciaCorp\Autenticacao\Autenticacao;
use ConferenciaCorp\Autenticacao\User\User;
use PHPUnit_Framework_TestCase as PHPUnit;

class AutenticacaoTest extends PHPUnit
{
    private $alg;
    private $tokenBuilder;
    private $autenticacao;

    public function setUp()
    {
        $this->alg = Common::ALG;

        $this->tokenBuilder = new TokenBuilder(Common::SECRET_KEY, $this->alg);
        $this->autenticacao = new Autenticacao(Common::SECRET_KEY, $this->alg);
    }

    public function testShouldBeAValidToken()
    {
        $token = $this->tokenBuilder->get();

        $this->autenticacao->setToken($token);

        $this->assertTrue($this->autenticacao->isValid());
    }

    public function testShouldNotBeAValidToken()
    {
        $invalidTokenBuilder = new TokenBuilder('notConferenciaCorp', $this->alg);
        $invalidToken = $invalidTokenBuilder->get();

        $this->autenticacao->setToken($invalidToken);

        $this->assertNotTrue($this->autenticacao->isValid());
    }

    public function testShouldBeAExpiredToken()
    {
        $token = $this->tokenBuilder->getTokenExpired();

        $this->autenticacao->setToken($token);

        $this->assertNotTrue($this->autenticacao->isValid());
    }

    public function testMustReturnAnInstanceOfUser()
    {
        $token = $this->tokenBuilder->get();

        $this->autenticacao->setToken($token);

        $this->assertInstanceOf(User::class, $this->autenticacao->getUser());
    }
}