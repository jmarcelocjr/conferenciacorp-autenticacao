<?php

namespace ConferenciaCorp\Autenticacao\User;

use ConferenciaCorp\Autenticacao\User\User;
use PHPUnit_Framework_TestCase as PHPUnit;

class UserTest extends PHPUnit
{
    private $user;
    private $payload;

    public function setUp()
    {
        $this->payload = [
            'nome' => 'Marcelo',
            'acl' => ['developer'],
            'data' => [
                'company' => 'ConferenciaCorp'
            ]
        ];

        $this->user = new User($this->payload);
    }

    public function testShouldReturnInstanceOfUser()
    {
        $this->assertInstanceOf(User::class, $this->user);
    }

    public function testShouldReturnInstanceOfUserWithoutData()
    {
        $user = $this->getUserWithoutData();

        $this->assertInstanceOf(User::class, $user);
    }

    private function getUserWithoutData()
    {
        $payloadWithoutData = $this->payload;

        unset($payloadWithoutData['data']);

        return new User($payloadWithoutData);
    }

    public function testShouldReturnDataEmpty()
    {
        $user = $this->getUserWithoutData();

        $this->assertTrue(empty($user->getData()));
    }

    public function testShouldAllowAcl()
    {
        $this->assertTrue($this->user->can('developer'));
    }

    public function testShouldNotAllowAcl()
    {
        $this->assertNotTrue($this->user->can('admin'));
    }

    public function testShouldReturnName()
    {
        $this->assertEquals('Marcelo', $this->user->getNome());
    }

    public function testShouldReturnAcls()
    {
        $this->assertInternalType('array', $this->user->getAcl());
    }

    public function testShouldReturnData()
    {
        $this->assertInternalType('array', $this->user->getData());
    }

    public function testShouldGetCompanyFromData()
    {
        $this->assertEquals('ConferenciaCorp', $this->user->company);
    }

    public function testShouldThrowExceptionWhenCallPropertiesThatNotExists()
    {
        $this->expectException(\Exception::class);

        $age = $this->user->age;
    }
}