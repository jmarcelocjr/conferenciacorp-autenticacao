<?php

namespace ConferenciaCorp\Autenticacao\User;

class User implements \JsonSerializable
{
    protected $nome;
    protected $acl;
    protected $data;

    public function __construct($payload)
    {
        $this->nome = $payload['nome'];
        $this->acl = $payload['acl'];

        $this->data = (isset($payload['data'])? $payload['data'] : []);
    }

    public function can($acl)
    {
        return in_array($acl, $this->acl) || in_array('admin', $this->acl);
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getAcl()
    {
        return $this->acl;
    }

    public function getData()
    {
        return $this->data;
    }

    public function __get($prop)
    {
        if(isset($this->data[$prop])){
            return $this->data[$prop];
        }

        throw new \Exception("Propriedade não encontrada");
    }

    public function jsonSerialize()
    {
        return [
            'nome' => $this->nome,
            'acl'  => $this->acl,
            'data' => $this->data
        ];
    }
}